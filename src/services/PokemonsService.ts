import { PokemonI } from "../interfaces/PokemonInterfaces"
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
const dbPokemons = require('../db/Pokemons.json');
const dbTypes = require('../db/Type.json')

module PokemonsService{
    export function against(attacking: number, defending: number): string{
        const types: Array<MonsterTypeI> = dbTypes;
        const pokemonOne: PokemonI = getById(attacking);
        const pokemonTwo: PokemonI = getById(defending);
        let calculatedMultipliers: number = 1;

        pokemonOne.type.forEach(p=>{
            let found = types.find(t=>t.id===p.id);
            if(found){
                let pokemonTwoTypes = pokemonTwo.type.map(t=>t.id);
                if(found.immunes.find(i=>pokemonTwoTypes.includes(i.id))){
                    calculatedMultipliers = calculatedMultipliers * 0;
                } else if (found.strengths.find(i=>pokemonTwoTypes.includes(i.id))){
                    calculatedMultipliers = calculatedMultipliers * 2;
                } else if (found.weaknesses.find(i=>pokemonTwoTypes.includes(i.id))){
                    calculatedMultipliers = calculatedMultipliers * (1/2)
                }
                else {
                    calculatedMultipliers = calculatedMultipliers * 1;
                }
            }
        });

        let status = calculatedMultipliers === 1 ? "normal" : (calculatedMultipliers > 1 ? "débil": "resistente");

        return `${pokemonTwo.name} es ${status} contra ${pokemonOne.name} (multiplier: ${calculatedMultipliers})`;
    }

    export function getById(id: number): PokemonI {
        const pokemons: Array<PokemonI> = dbPokemons;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el pokemon"
        }
        return pokemon[0];
    }

    export function add(pokemon: PokemonI): PokemonI[] {
        const pokemonsFull: Array<PokemonI> = dbPokemons;
        //const s: Array<DigimonI> = digimons.filter(e => e.name.toLowerCase().includes(name.toLowerCase()));
        let exists = pokemonsFull.filter(d => d.id === pokemon.id).length > 0;
        if(exists){
            throw "El id ya existe";
        }
        pokemonsFull.push(pokemon);
        return pokemonsFull;
    }

    export function getByType(type: string): PokemonI[] {
        const pokemons: Array<PokemonI> = dbPokemons;
        const pokemon: Array<PokemonI> = [];
        pokemons.forEach(d=>{
            d.type.forEach(t=>{
                if(t.name.toLowerCase().includes(type.toLowerCase())){
                    pokemon.push(d);
                }
            });
        });

        if (pokemon.length < 1) {
            throw "No se encontró el pokemon"
        }
        return pokemon;
    }

    export function getAll(): Array<PokemonI> {
        const pokemons: Array<PokemonI> = dbPokemons;
        return pokemons;
    }

    export function getByName(name: string): PokemonI[] {
        const pokemons: Array<PokemonI> = dbPokemons;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.name.toLowerCase().includes(name.toLowerCase()));
        if (pokemon.length < 1) {
            throw "No se encontró el pokemon"
        }
        return pokemon;
    }

}
export default PokemonsService;