import { Request, Response } from "express";
import PokemonsService from "../services/PokemonsService";

export function getAll(_: any, res: Response) {
    const pokemons = PokemonsService.getAll();
    res.status(200).json(pokemons);
}

export function getById(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del pokemon."}
        const pokemon = PokemonsService.getById(id);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type || undefined;
        if(!type){ throw "Se requiere el tipo del pokemon."}
        const pokemon = PokemonsService.getByType(type);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function against(req: Request, res: Response) {
    try {
        const attacking = req.params.attacking && +req.params.attacking || undefined;
        if(!attacking){ throw "Se requiere el ID del pokemon atacante."} 
        const defending = req.params.defending && +req.params.defending || undefined;
        if(!defending){ throw "Se requiere el ID del pokemon defensor."} 
        const pokemonMultiplier = PokemonsService.against(attacking, defending);
        res.status(200).json(pokemonMultiplier);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function add(req: Request, res: Response) {
    try {
        const pokemon = req.body || undefined;
        if(!pokemon || (Object.keys(pokemon).length === 0 && pokemon.constructor === Object)){
             throw "Se requiere pokemon a ingresar."
        }
        const pokemonFull = PokemonsService.add(pokemon);
        res.status(200).json(pokemonFull);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name || undefined;
        if(!name){ throw "Se requiere el nombre del pokemon."}
        const pokemon = PokemonsService.getByName(name);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}