import { Request, Response } from "express";
import DigimonsService from "../services/DigimonsService";

export function getAll(_: any, res: Response) {
    const digimons = DigimonsService.getAll();
    res.status(200).json(digimons);
}

export function getById(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del digimon."}
        const digimon = DigimonsService.getById(id);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name || undefined;
        if(!name){ throw "Se requiere el nombre del digimon."}
        const digimon = DigimonsService.getByName(name);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type || undefined;
        if(!type){ throw "Se requiere el tipo del digimon."}
        const digimon = DigimonsService.getByType(type);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function add(req: Request, res: Response) {
    try {
        const digimon = req.body || undefined;
        if(!digimon || (Object.keys(digimon).length === 0 && digimon.constructor === Object)){
             throw "Se requiere digimon a ingresar."
        }
        const digimonFull = DigimonsService.add(digimon);
        res.status(200).json(digimonFull);
    } catch (error) {
        res.status(400).send(error);
    }
}
