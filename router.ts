import express from 'express';
import * as DigimonsController from './src/controllers/DigimonsController';
import * as PokemonsController from './src/controllers/PokemonsController';

export const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World with Typescript!')
})

router.get('/ts', (req, res) => {
    res.send('Typescript es lo máximo!')
})

router.get('/digimons', DigimonsController.getAll);
router.get('/digimons/id/:id', DigimonsController.getById);
router.get('/digimons/name/:name', DigimonsController.getByName);
router.get('/digimons/type/:type', DigimonsController.getByType);
router.post('/digimons/add', DigimonsController.add);

router.get('/pokemons', PokemonsController.getAll);
router.get('/pokemons/id/:id', PokemonsController.getById);
router.get('/pokemons/name/:name', PokemonsController.getByName);
router.get('/pokemons/type/:type', PokemonsController.getByType);
router.post('/pokemons/add', DigimonsController.add);
//TODO
router.get('/pokemons/against/:attacking/:defending', PokemonsController.against);

router.post("/", (req, res) => {
    console.log("Cuerpo:", req.body);
    res.status(200).send(req.body);
});
